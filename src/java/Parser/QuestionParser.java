/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Parser;

/**
 *
 * @author Adrian Paul
 */
import Bean.Question;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class QuestionParser {
    
    
    public Question QuestionParseFeed(String response) {
        try {
            JSONObject obj = new JSONObject(response);
            Question Question = new Question();
            if (obj.has("id")) {
                if (!obj.isNull("id")) {
                    Question.setId(obj.getInt("id") + "");
                } else {
                    Question.setId("-");
                }
            }

            if (obj.has("id")) {
                    if (!obj.isNull("id")) {
                        Question.setId(obj.getInt("id") + "");
                    } else {
                        Question.setId("-");
                    }
                }

                if (obj.has("question")) {
                    if (!obj.isNull("question")) {
                        Question.setQuestion(obj.getString("question") + "");
                    } else {
                        Question.setQuestion("-");
                    }
                }

                if (obj.has("questionId")) {
                    if (!obj.isNull("questionId")) {
                        Question.setQuestionId(obj.getInt("questionId") + "");
                    } else {
                        Question.setQuestionId("-");
                    }
                }
                
                
                if (obj.has("questionnaireTypeIdFk")) {
                    if (!obj.isNull("questionnaireTypeIdFk")) {
                        Question.setQuestionnaireTypeIdFk(obj.getInt("questionnaireTypeIdFk") + "");
                    } else {
                        Question.setQuestionnaireTypeIdFk("-");
                    }
                }
                
                if (obj.has("answerTypeIdFk")) {
                    if (!obj.isNull("answerTypeIdFk")) {
                        Question.setAnswerTypeIdFk(obj.getInt("answerTypeIdFk") + "");
                    } else {
                        Question.setAnswerTypeIdFk("-");
                    }
                }
                

            return Question;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    
    public ArrayList<Question> parseAllQuestion(String response) {
        try {
            JSONArray ar = new JSONArray(response);
            ArrayList<Question> Questions = new ArrayList<>();
            for (int i = 0; i < ar.length(); i++) {
                JSONObject obj = ar.getJSONObject(i);
                Question Question = new Question();

                if (obj.has("id")) {
                    if (!obj.isNull("id")) {
                        Question.setId(obj.getInt("id") + "");
                    } else {
                        Question.setId("-");
                    }
                }

                if (obj.has("question")) {
                    if (!obj.isNull("question")) {
                        Question.setQuestion(obj.getString("question") + "");
                    } else {
                        Question.setQuestion("-");
                    }
                }

                if (obj.has("questionId")) {
                    if (!obj.isNull("questionId")) {
                        Question.setQuestionId(obj.getInt("questionId") + "");
                    } else {
                        Question.setQuestionId("-");
                    }
                }
                
                
                if (obj.has("questionnaireTypeIdFk")) {
                    if (!obj.isNull("questionnaireTypeIdFk")) {
                        Question.setQuestionnaireTypeIdFk(obj.getInt("questionnaireTypeIdFk") + "");
                    } else {
                        Question.setQuestionnaireTypeIdFk("-");
                    }
                }
                
                if (obj.has("answerTypeIdFk")) {
                    if (!obj.isNull("answerTypeIdFk")) {
                        Question.setAnswerTypeIdFk(obj.getInt("answerTypeIdFk") + "");
                    } else {
                        Question.setAnswerTypeIdFk("-");
                    }
                }
                

                Questions.add(Question);
            }
            return Questions;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    
}
