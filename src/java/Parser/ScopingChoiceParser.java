/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Parser;

/**
 *
 * @author Adrian Paul
 */
import Bean.ScopingChoice;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ScopingChoiceParser {
    
    
    public ScopingChoice ScopingChoiceParseFeed(String response) {
        try {
            JSONObject obj = new JSONObject(response);
            ScopingChoice ScopingChoice = new ScopingChoice();
            if (obj.has("id")) {
                if (!obj.isNull("id")) {
                    ScopingChoice.setId(obj.getInt("id") + "");
                } else {
                    ScopingChoice.setId("-");
                }
            }

            if (obj.has("id")) {
                    if (!obj.isNull("id")) {
                        ScopingChoice.setId(obj.getInt("id") + "");
                    } else {
                        ScopingChoice.setId("-");
                    }
                }

                if (obj.has("choice")) {
                    if (!obj.isNull("choice")) {
                        ScopingChoice.setChoice(obj.getString("choice") + "");
                    } else {
                        ScopingChoice.setChoice("-");
                    }
                }

                if (obj.has("questionIdFk")) {
                    if (!obj.isNull("questionIdFk")) {
                        ScopingChoice.setQuestionIdFk(obj.getInt("questionIdFk") + "");
                    } else {
                        ScopingChoice.setQuestionIdFk("-");
                    }
                }
                

            return ScopingChoice;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    
    public ArrayList<ScopingChoice> parseAllScopingChoice(String response) {
        try {
            JSONArray ar = new JSONArray(response);
            ArrayList<ScopingChoice> ScopingChoices = new ArrayList<>();
            for (int i = 0; i < ar.length(); i++) {
                JSONObject obj = ar.getJSONObject(i);
                ScopingChoice ScopingChoice = new ScopingChoice();

                if (obj.has("id")) {
                    if (!obj.isNull("id")) {
                        ScopingChoice.setId(obj.getInt("id") + "");
                    } else {
                        ScopingChoice.setId("-");
                    }
                }

                if (obj.has("choice")) {
                    if (!obj.isNull("choice")) {
                        ScopingChoice.setChoice(obj.getString("choice") + "");
                    } else {
                        ScopingChoice.setChoice("-");
                    }
                }

                if (obj.has("questionIdFk")) {
                    if (!obj.isNull("questionIdFk")) {
                        ScopingChoice.setQuestionIdFk(obj.getInt("questionIdFk") + "");
                    } else {
                        ScopingChoice.setQuestionIdFk("-");
                    }
                }
                
                
                

                ScopingChoices.add(ScopingChoice);
            }
            return ScopingChoices;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    
}
