/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Parser;

/**
 *
 * @author Adrian Paul
 */
import Bean.PdfUrl;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PdfUrlParser {
    
    
    public PdfUrl pdfUrlParser(String response) {
        try {
            JSONObject obj = new JSONObject(response);
            PdfUrl PdfUrl = new PdfUrl();
            if (obj.has("url")) {
                if (!obj.isNull("url")) {
                    PdfUrl.setUrl(obj.getString("url") + "");
                } else {
                    PdfUrl.setUrl("-");
                }
            }


            return PdfUrl;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    
    public ArrayList<PdfUrl> parseAllPdfUrlParser(String response) {
        try {
            JSONArray ar = new JSONArray(response);
            ArrayList<PdfUrl> PdfUrls = new ArrayList<>();
            for (int i = 0; i < ar.length(); i++) {
                JSONObject obj = ar.getJSONObject(i);
                PdfUrl PdfUrl = new PdfUrl();

                if (obj.has("url")) {
                    if (!obj.isNull("url")) {
                        PdfUrl.setUrl(obj.getString("url") + "");
                    } else {
                        PdfUrl.setUrl("-");
                    }
                }

               
                
                
                

                PdfUrls.add(PdfUrl);
            }
            return PdfUrls;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    
}
