/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

/**
 *
 * @author zarsantos
 */
public class ScopingChoice {
    private String id;
    private String choice;
    private String questionIdFk;

    
    
   
    public  ScopingChoice(){
    }
    
    public ScopingChoice(String id,String choice,String questionIdFk){
        this.id= id;
        this.choice= choice;
        this.questionIdFk= questionIdFk;

    }
    
     public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    public String getChoice() {
        return choice;
    }

    public void setChoice(String choice) {
        this.choice = choice;
    }
    
    public String getQuestionIdFk() {
        return questionIdFk;
    }

    public void setQuestionIdFk(String questionIdFk) {
        this.questionIdFk = questionIdFk;
    }
    
   
    
}
