/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

/**
 *
 * @author zarsantos
 */
public class Question {
    private String id;
    private String answerTypeIdFk;
    private String questionId;
    private String questionnaireTypeIdFk;
    private String question;
    private String lastUpdate;

    
    
   
    public  Question(){
    }
    
    public Question(String id,String questionId,String answerTypeIdFk,String questionnaireTypeIdFk, String question, String lastUpdate){
        this.id= id;
        this.questionId= questionId;
        this.answerTypeIdFk= answerTypeIdFk;
        this.questionnaireTypeIdFk= questionnaireTypeIdFk;
        this.question= question;
        this.lastUpdate= lastUpdate;
  

    
       

    }
    
     public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    public String getAnswerTypeIdFk() {
        return answerTypeIdFk;
    }

    public void setAnswerTypeIdFk(String answerTypeIdFk) {
        this.answerTypeIdFk = answerTypeIdFk;
    }
    
    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }
    
    
     
    public String getQuestionnaireTypeIdFk() {
        return questionnaireTypeIdFk;
    }

    public void setQuestionnaireTypeIdFk(String questionnaireTypeIdFk) {
        this.questionnaireTypeIdFk = questionnaireTypeIdFk;
    }
    
     public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }
    
    
    public String getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }
    
           
}
