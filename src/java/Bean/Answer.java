/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import org.json.JSONArray;

/**
 *
 * @author Adrian Paul
 */
public class Answer {
    
    private JSONArray scopingQuestionAndAnswerList;
    private String firstname;
    private String lastname;
    
    private String email;
    private String contactNumber;
    private String pdfLink;
    private String digitalProfile;
    private String companyName;
    
    
   
    public  Answer(){
    }
    
    public Answer(JSONArray scopingQuestionAndAnswerList,String firstname,String lastname, String email, String contactNumber, String pdfLink, String digitalProfile,String companyName){
        this.scopingQuestionAndAnswerList= scopingQuestionAndAnswerList;
        this.firstname= firstname;
        this.lastname = lastname;
        this.email = email;
        this.contactNumber = contactNumber;
        this.pdfLink = pdfLink;
        this.digitalProfile = digitalProfile;
        this.companyName = companyName;
    }
    
     public JSONArray getScopingQuestionAndAnswerList() {
        return scopingQuestionAndAnswerList;
    }

    public void setScopingQuestionAndAnswerList(JSONArray scopingQuestionAndAnswerList) {
        this.scopingQuestionAndAnswerList = scopingQuestionAndAnswerList;
    }
    
   
    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }
    
     public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
    
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    
    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber= contactNumber;
    }
    
    
    public String getPdfLink() {
        return pdfLink;
    }

    public void setPdfLink(String pdfLink) {
        this.pdfLink= pdfLink;
    }
    
    
    public String getDigitalProfile() {
        return digitalProfile;
    }

    public void setDigitalProfile(String digitalProfile) {
        this.digitalProfile= digitalProfile;
    }
    
    
    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName= companyName;
    }
    
}
    

