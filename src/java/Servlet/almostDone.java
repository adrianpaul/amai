
        /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import Bean.Question;
import Bean.ScopingChoice;
import HTTPManagement.HTTPManagerDirectory;
import Others.Constants;
import Parser.QuestionParser;
import Parser.ScopingChoiceParser;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author zarsantos
 */
@WebServlet(name = "almostDone", urlPatterns = {"/almostDone"})
public class almostDone extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
          HttpSession session = request.getSession();
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String goal = request.getParameter("goals");
            String connectivitySetup = request.getParameter("connectivitySetups");
            String itSetup = request.getParameter("itSetups");
            String problemsEncounter = request.getParameter("problemsEncounters");
            String mindset = request.getParameter("mindsets");
           
            int lateComerCount =0;
            int frontRunnerCount =0;
            
          
            
            
            System.out.println("lateComerCount: "+lateComerCount);
            System.out.println("frontRunnerCount: "+frontRunnerCount);
            
            String digitalProfile = "";
            
            if(problemsEncounter.equalsIgnoreCase("Ive considered technology but havent tried it out yet") || problemsEncounter.equalsIgnoreCase("I pay someone who can recommend and/or manage more advanced solutions for my business")){
                lateComerCount++;
            }
            
            if(problemsEncounter.equalsIgnoreCase("I research and use free simple solutions for my business") || problemsEncounter.equalsIgnoreCase("I explore and pay for customized apps and software specific to my needs for my business")){
                frontRunnerCount++;
            }
            
            if(mindset.equalsIgnoreCase("Its costly and I would rather see others try first") || mindset.equals("I will only explore new technology when I encounter major challenges with my tried and tested solutions")){
               lateComerCount++;
            }
            
            if(mindset.equalsIgnoreCase("If it means helping my business grow faster and is within my budget Im all in") || mindset.equals("Simply put its a worthy investment")){
               frontRunnerCount++;
            }
            
            
            if(itSetup.equalsIgnoreCase("My IT is managed by the owner or a non-technical employee usually on top of other duties") || itSetup.equalsIgnoreCase("My IT is managed by an outsourced technical team")){
                lateComerCount++;
            }
            
            if(itSetup.equalsIgnoreCase("My IT is managed by one dedicated technical person") || itSetup.equalsIgnoreCase("My IT is managed by a team of dedicated technical employees")){
                frontRunnerCount++;
            }
            
            if(connectivitySetup.equalsIgnoreCase("I use a high quality dedicated leased internet line for work") || connectivitySetup.equalsIgnoreCase("I use an office broadband for work that is less than 20mbps")){
                lateComerCount++;
            }
            
            if(connectivitySetup.equalsIgnoreCase("I use an office broadband for work that is greater than 20mbps") || connectivitySetup.equalsIgnoreCase("I use my home broadband for work and business")){
                frontRunnerCount++;
            }
            
         
            
           if(lateComerCount>frontRunnerCount){
           
               digitalProfile ="digital latecomer";
           }else{
                digitalProfile ="digital frontrunner";
           }
           
            String question5 = request.getParameter("question5");
            String question6 = request.getParameter("question6");
            String question7 = request.getParameter("question7");
            String question8 = request.getParameter("question8");
            String question9 = request.getParameter("question9");
            
            ArrayList goalList= new ArrayList(Arrays.asList(goal.split(",")));

            String goal1 = goalList.get(0).toString();
            String goal2 = goalList.get(1).toString();
            String goal3 = goalList.get(2).toString();
            
            String goals1 = "";
            String goals2 = "";
            String goals3 = "";
            
            if(goal1.equalsIgnoreCase("Win more customers")){
                goals1 ="Customer Acquisition";
            }
           
            if(goal1.equalsIgnoreCase("Keep customers coming back")){
                goals1 ="Customer Loyalty";
            }
            
             
            if(goal1.equalsIgnoreCase("Work anytime anywhere / Work on the go")){
                goals1 ="Future Ready WorkForce";
            }
            
              
            if(goal1.equalsIgnoreCase("Stable internet connection")){
                goals1 ="Connectivity";
            }
            
            if(goal1.equalsIgnoreCase("Level up work apps for increased productivity")){
                goals1 ="Infrastructure";
            }
            
            if(goal1.equalsIgnoreCase("Secure important and private business data")){
                goals1 ="Security";
            }
            
            if(goal1.equalsIgnoreCase("Speed up and secure payment transactions")){
                goals1 ="Payment Channels";
            }
            
            if(goal1.equalsIgnoreCase("Gain digital training")){
                goals1 ="Training / Upskilling";
            }
            
            
            if(goal2.equalsIgnoreCase("Win more customers")){
                goals2 ="Customer Acquisition";
            }
           
            if(goal2.equalsIgnoreCase("Keep customers coming back")){
                goals2 ="Customer Loyalty";
            }
            
             
            if(goal2.equalsIgnoreCase("Work anytime anywhere / Work on the go")){
                goals2 ="Future Ready WorkForce";
            }
            
              
            if(goal2.equalsIgnoreCase("Stable internet connection")){
                goals2 ="Connectivity";
            }
            
            if(goal2.equalsIgnoreCase("Level up work apps for increased productivity")){
                goals2 ="Infrastructure";
            }
            
            if(goal2.equalsIgnoreCase("Secure important and private business data")){
                goals2 ="Security";
            }
            
            if(goal2.equalsIgnoreCase("Speed up and secure payment transactions")){
                goals2 ="Payment Channels";
            }
            
            if(goal2.equalsIgnoreCase("Gain digital training")){
                goals2 ="Training / Upskilling";
            }
            
        
            if(goal3.equalsIgnoreCase("Win more customers")){
                goals3 ="Customer Acquisition";
            }
           
            if(goal3.equalsIgnoreCase("Keep customers coming back")){
                goals3 ="Customer Loyalty";
            }
            
             
            if(goal3.equalsIgnoreCase("Work anytime anywhere / Work on the go")){
                goals3 ="Future Ready WorkForce";
            }
            
              
            if(goal3.equalsIgnoreCase("Stable internet connection")){
                goals3 ="Connectivity";
            }
            
            if(goal3.equalsIgnoreCase("Level up work apps for increased productivity")){
                goals3 ="Infrastructure";
            }
            
            if(goal3.equalsIgnoreCase("Secure important and private business data")){
                goals3 ="Security";
            }
            
            if(goal3.equalsIgnoreCase("Speed up and secure payment transactions")){
                goals3 ="Payment Channels";
            }
            
            if(goal3.equalsIgnoreCase("Gain digital training")){
                goals3 ="Training / Upskilling";
            }
            
            String goalId1 = "";
            String goalId2 = "";
            String goalId3 = "";
            String itSetupId = "";
            String connectivitySetupId = "";
            String problemsEncounterId = "";
            String mindsetId = "";
            String question5Id = "";
            
            String question6Id = "";
            String question7Id = "";
            String question8Id = "";
            String question9Id = "";
            
            
            ArrayList<ScopingChoice> scopingChoice = new ArrayList<ScopingChoice>();
            ScopingChoiceParser scopingChoiceParser = new ScopingChoiceParser();
            ArrayList<Question> question = new ArrayList<Question>();
            QuestionParser questionParser = new QuestionParser();
            
           
            HTTPManagerDirectory tp = new HTTPManagerDirectory();
            String endPoint = Constants.serverURL;
            String email = Constants.email;
            String password = Constants.password;
            
            URI uriSample = null;
            String getAllScopingChoice = endPoint + "survey/scopingchoice";
            System.out.println("getAllScopingChoice: "+getAllScopingChoice);
            String finalUrls ="";
              uriSample = null;
              try {
                uriSample = new URI(getAllScopingChoice);
                finalUrls = uriSample.toURL().toString();

            } catch (URISyntaxException ex) {
                finalUrls = endPoint + "survey/scopingchoice";
            }

            String allScopingChoiceResponse = tp.executeGetRequest(finalUrls, "GET", email, password);

            System.out.println("allScopingChoiceResponse: "+allScopingChoiceResponse);
            scopingChoice = scopingChoiceParser.parseAllScopingChoice(allScopingChoiceResponse);
              
           for(int i=0; i<scopingChoice.size();i++){
               
            
               if(goal1.equalsIgnoreCase(scopingChoice.get(i).getChoice())){
                   goalId1  = scopingChoice.get(i).getId();
               }
               
               if(goal2.equalsIgnoreCase(scopingChoice.get(i).getChoice())){
                   goalId2  = scopingChoice.get(i).getId();
               }
               
               if(goal3.equalsIgnoreCase(scopingChoice.get(i).getChoice())){
                   goalId3  = scopingChoice.get(i).getId();
               }
            
            
               if(itSetup.equalsIgnoreCase(scopingChoice.get(i).getChoice())){
                 itSetupId  = scopingChoice.get(i).getId();
               }
               
               if(connectivitySetup.equalsIgnoreCase(scopingChoice.get(i).getChoice())){
                 connectivitySetupId  = scopingChoice.get(i).getId();
               }
               
                if(problemsEncounter.equalsIgnoreCase(scopingChoice.get(i).getChoice())){
                  problemsEncounterId  = scopingChoice.get(i).getId();
                }
                
                if(mindset.equalsIgnoreCase(scopingChoice.get(i).getChoice())){
                   mindsetId  = scopingChoice.get(i).getId();
               }
            }
            
            
            String getAllQuestion= endPoint + "survey/scopingquestion";
            System.out.println("getAllQuestion: "+getAllQuestion);
            String finalQuestionUrls ="";
              uriSample = null;
              try {
                uriSample = new URI(getAllQuestion);
                finalQuestionUrls = uriSample.toURL().toString();

            } catch (URISyntaxException ex) {
                finalQuestionUrls = endPoint + "survey/scopingquestion";
            }


            String allQuestionResponse = tp.executeGetRequest(finalQuestionUrls, "GET", email, password);
        
       
        System.out.println("question5: "+question5);
        System.out.println("question6: "+question6);
        System.out.println("question7: "+question7);
        System.out.println("question8: "+question8);
        System.out.println("question9: "+question9);
        
        
            System.out.println("allQuestionResponse: "+allQuestionResponse);
            question = questionParser.parseAllQuestion(allQuestionResponse);
              
            for(int i=0; i<question.size();i++){
                if(question5.equalsIgnoreCase(question.get(i).getQuestion())){
                  question5Id  = question.get(i).getQuestionId();
               }
                
               if(question6.equalsIgnoreCase(question.get(i).getQuestion())){
                  question6Id  = question.get(i).getQuestionId();
               }
               if(question7.equalsIgnoreCase(question.get(i).getQuestion())){
                    question7Id  = question.get(i).getQuestionId();
               }
            
               if(question8.equalsIgnoreCase(question.get(i).getQuestion())){
                  question8Id  = question.get(i).getQuestionId();
               }
               
                 
                  
                if(question9.equalsIgnoreCase(question.get(i).getQuestion())){
                  question9Id  = question.get(i).getId();
               }   
            
           }
           
            System.out.println("goal: "+goal);
            System.out.println("itSetup: "+itSetup);
            System.out.println("connectivitySetup: "+connectivitySetup);
            System.out.println("problemsEncounter: "+problemsEncounter);
            System.out.println("mindset: "+mindset);
            
            
            System.out.println("question5Id: "+question5Id);
            System.out.println("question6Id: "+question6Id);
            System.out.println("question7Id: "+question7Id);
            System.out.println("question8Id: "+question8Id);
            System.out.println("question9Id: "+question9Id);
                   
            System.out.println("goal1: "+goal1);
            System.out.println("goal2: "+goal2);
            System.out.println("goal3: "+goal3);
            System.out.println("goalId1: "+goalId1);
            System.out.println("goalId2: "+goalId2);
            System.out.println("goalId3: "+goalId3);
            
            System.out.println("connectivitySetupId: "+connectivitySetupId);
            System.out.println("problemsEncounterId: "+problemsEncounterId);
            System.out.println("mindsetId: "+mindsetId);
            System.out.println("itSetupId: "+itSetupId);
            
            
            session.setAttribute("goal", goal);
            session.setAttribute("goal1", goal1);
            session.setAttribute("goal2", goal2);
            session.setAttribute("goal3", goal3);
            session.setAttribute("goals1", goals1);
            session.setAttribute("goals2", goals2);
            session.setAttribute("goals3", goals3);
            
            session.setAttribute("connectivitySetup", connectivitySetup);
            session.setAttribute("itSetup", itSetup);
            session.setAttribute("problemsEncounter", problemsEncounter);
            session.setAttribute("mindset", mindset);
          
             session.setAttribute("connectivitySetupId", connectivitySetupId);
            session.setAttribute("itSetupId", itSetupId);
            session.setAttribute("problemsEncounterId", problemsEncounterId);
            session.setAttribute("mindsetId", mindsetId);
            
            
            session.setAttribute("goalId1", goalId1);
            session.setAttribute("goalId2", goalId2);
            session.setAttribute("goalId3", goalId3);
            session.setAttribute("question6", question6);
            session.setAttribute("question7", question7);
            session.setAttribute("question8", question8);
            session.setAttribute("question9", question8);
            
            
            session.setAttribute("question5Id", question5Id);
            session.setAttribute("question6Id", question6Id);
            session.setAttribute("question7Id", question7Id);
            session.setAttribute("question8Id", question8Id);
            session.setAttribute("question9Id", question9Id);
            session.setAttribute("profile", digitalProfile);
             
            session.setAttribute("lateComerCount", lateComerCount);
            session.setAttribute("frontRunnerCount", frontRunnerCount);
            
            
            
            
            
            response.sendRedirect("almostDone.jsp");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
