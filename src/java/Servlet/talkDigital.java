/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import Bean.Question;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import Bean.ScopingChoice;
import HTTPManagement.HTTPManagerDirectory;
import Others.Constants;
import Parser.QuestionParser;
import Parser.ScopingChoiceParser;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author zarsantos
 */
@WebServlet(name = "talkDigital", urlPatterns = {"/talkDigital"})
public class talkDigital extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, URISyntaxException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String industry = request.getParameter("industries");
            String workForceSize = request.getParameter("workForceSizes");
            String role = request.getParameter("roles");
            String relate = request.getParameter("relates");
            
          
            
           
            String question1 = request.getParameter("question1");
            String question2 = request.getParameter("question2");
            String question3 = request.getParameter("question3");
            String question4 = request.getParameter("question4");
            
           
            
            
            
          
            
            String industryId = "";
            String workForceSizeId = "";
            String roleId = "";
            String relateId = "";
            
            
            
            String question1Id = "";
            String question2Id = "";
            String question3Id = "";
            String question4Id = "";
            
         
            
            ArrayList<ScopingChoice> scopingChoice = new ArrayList<ScopingChoice>();
            ScopingChoiceParser scopingChoiceParser = new ScopingChoiceParser();
            
            ArrayList<Question> question = new ArrayList<Question>();
            QuestionParser questionParser = new QuestionParser();
            
            HTTPManagerDirectory tp = new HTTPManagerDirectory();
            String endPoint = Constants.serverURL;
            String email = Constants.email;
            String password = Constants.password;
            
      
            
            URI uriSample = null;
            String getAllScopingChoice = endPoint + "survey/scopingchoice";
            System.out.println("getAllScopingChoice: "+getAllScopingChoice);
            String finalUrls ="";
              uriSample = null;
              try {
                uriSample = new URI(getAllScopingChoice);
                finalUrls = uriSample.toURL().toString();

            } catch (URISyntaxException ex) {
                finalUrls = endPoint + "survey/scopingchoice";
            }


            String allScopingChoiceResponse = tp.executeGetRequest(finalUrls, "GET", email, password);

            System.out.println("allScopingChoiceResponse: "+allScopingChoiceResponse);
            scopingChoice = scopingChoiceParser.parseAllScopingChoice(allScopingChoiceResponse);
              
           for(int i=0; i<scopingChoice.size();i++){
               
              
               if(industry.equalsIgnoreCase(scopingChoice.get(i).getChoice())){
                    industryId  = scopingChoice.get(i).getId();
               }
            
               if(role.equalsIgnoreCase(scopingChoice.get(i).getChoice())){
                  roleId  = scopingChoice.get(i).getId();
               }
                if(workForceSize.equalsIgnoreCase(scopingChoice.get(i).getChoice())){
                   workForceSizeId  = scopingChoice.get(i).getId();
               }
                
                 if(relate.equalsIgnoreCase(scopingChoice.get(i).getChoice())){
                   relateId  = scopingChoice.get(i).getId();
               }
                
           }
           
            
            
            
            String getAllQuestion= endPoint + "survey/scopingquestion";
            System.out.println("getAllQuestion: "+getAllQuestion);
            String finalQuestionUrls ="";
              uriSample = null;
              try {
                uriSample = new URI(getAllQuestion);
                finalQuestionUrls = uriSample.toURL().toString();

            } catch (URISyntaxException ex) {
                finalQuestionUrls = endPoint + "survey/scopingquestion";
            }


            String allQuestionResponse = tp.executeGetRequest(finalQuestionUrls, "GET", email, password);
            System.out.println("question1: "+question1);
            System.out.println("question2: "+question2);
            System.out.println("question3: "+question3);
            System.out.println("question4: "+question4);
            
            
            
            System.out.println("allQuestionResponse: "+allQuestionResponse);
            question = questionParser.parseAllQuestion(allQuestionResponse);
              
            for(int i=0; i<question.size();i++){
               
               if(question1.equalsIgnoreCase(question.get(i).getQuestion())){
                   question1Id  = question.get(i).getQuestionId();
               }
               if(question2.equalsIgnoreCase(question.get(i).getQuestion())){
                    question2Id  = question.get(i).getQuestionId();
               }
            
               if(question3.equalsIgnoreCase(question.get(i).getQuestion())){
                  question3Id  = question.get(i).getQuestionId();
               }
               
                if(question4.equalsIgnoreCase(question.get(i).getQuestion())){
                  question4Id  = question.get(i).getQuestionId();
               }
               
            
           }
            
            System.out.println("industry: "+industry);
            System.out.println("workForceSize: "+workForceSize);
            System.out.println("role: "+role);
            System.out.println("relate: "+relate);
            
          
            
            
            System.out.println("industryId: "+industryId);
            System.out.println("workForceSizeId: "+workForceSizeId);
            System.out.println("roleId: "+roleId);
            System.out.println("relateId: "+relateId);
            
            
         
            System.out.println("question1Id: "+question1Id);
            System.out.println("question2Id: "+question2Id);
            System.out.println("question3Id: "+question3Id);
            System.out.println("question4Id: "+question4Id);
            
            
            
            
            
            session.setAttribute("industry", industry);
            session.setAttribute("workForceSize", workForceSize);
            session.setAttribute("role", role);
            session.setAttribute("relate", relate);
            
            session.setAttribute("industryId", industryId);
            session.setAttribute("workForceSizeId", workForceSizeId);
            session.setAttribute("roleId", roleId);
            session.setAttribute("relateId", relateId);
            
          
            session.setAttribute("question1Id", question1Id);
            session.setAttribute("question2Id", question2Id);
            session.setAttribute("question3Id", question3Id);
            session.setAttribute("question4Id", question4Id);
            
           
            
            
            response.sendRedirect("digitalReadinessTest2A.jsp");
           
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (URISyntaxException ex) {
            Logger.getLogger(talkDigital.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (URISyntaxException ex) {
            Logger.getLogger(talkDigital.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
