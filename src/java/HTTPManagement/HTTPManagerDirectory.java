/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HTTPManagement;

import Bean.Answer;
import Others.Constants;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.HttpsURLConnection;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author sgmwpsupport
 */
public class HTTPManagerDirectory {

    public String executeRequest(String targetURL, String method, String email, String pw) throws MalformedURLException, IOException {
        HttpsTrustManager.allowAllSSL();
        final String user = email;
        final String password = pw;

        Base64.Encoder encoder = Base64.getEncoder();
        URL url = new URL(targetURL + email + "/");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        InputStream is = null;

        try {
            // Create connection
            connection.setReadTimeout(40000);
            connection.setConnectTimeout(40000);
            connection.setRequestMethod(method);
            connection.setDoInput(true);
            String credentials = user + ":" + password;

            connection.setRequestProperty("Content-Type", "application/json;");
            connection.setRequestProperty("Content-Language", "en-US");

            String base64EncodedCredentials = encoder.encodeToString(credentials.getBytes());
            connection.setRequestProperty("Authorization", "Basic " + base64EncodedCredentials);

            connection.connect();

            // Get Response  
            Integer responsecode = connection.getResponseCode();

            is = connection.getInputStream();

            String contentAsString = convertInputStreamToString(is);

            if (responsecode != 200 || "".equals(contentAsString)) {
                connection.disconnect();
                return "";
            } else {
                connection.disconnect();
                return contentAsString;
            }
        } catch (Exception e) {
            connection.disconnect();
            return "";
        } finally {
            if (is != null) {
                is.close();
            }
        }
    }
    
    
    public String executeGetRequest(String targetURL, String method, String email, String pw) throws MalformedURLException, IOException {
        HttpsTrustManager.allowAllSSL();
        final String user = email;
        final String password = pw;

        Base64.Encoder encoder = Base64.getEncoder();
        URL url = new URL(targetURL);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        InputStream is = null;

        try {
            // Create connection
            connection.setReadTimeout(50000);
            connection.setConnectTimeout(50000);
            connection.setRequestMethod(method);
            connection.setDoInput(true);
            String credentials = user + ":" + password;

            connection.setRequestProperty("Content-Type",
                    "application/json;");
            connection.setRequestProperty("Content-Language", "en-US");

            String base64EncodedCredentials = encoder.encodeToString(credentials.getBytes());
            connection.setRequestProperty("Authorization", "Basic " + base64EncodedCredentials);

            connection.connect();

            // Get Response  
            Integer responsecode = connection.getResponseCode();

            is = connection.getInputStream();

            String contentAsString = convertInputStreamToString(is);

//            System.out.println(targetURL + " || " + responsecode);
            if (responsecode != 200 || "".equals(contentAsString)) {
                connection.disconnect();
                return "";
            } else {
                return contentAsString;
            }
        } catch (Exception e) {
            connection.disconnect();
            e.printStackTrace();
            return "";
        } finally {
            if (is != null) {
                is.close();
            }
        }
    }
    
    
    public int executeAddAnswer(String targetURL, Answer answer, String method, String email, String password) throws IOException, ParseException, JSONException {
        HttpsTrustManager.allowAllSSL();

        String user = email;
        String pass = password;

        URL url = new URL(targetURL);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        InputStream is = null;
        String response = "";
        int responseCode =0;

       
        
        try {
            
            JSONObject userJson = new JSONObject();
            userJson.put("scopingQuestionAndAnswerList",answer.getScopingQuestionAndAnswerList());
            userJson.put("fname", answer.getFirstname().toUpperCase() + "");
            userJson.put("lname", answer.getLastname().toUpperCase() + "");
            
            userJson.put("email", answer.getEmail() + "");
            userJson.put("mobileNumber", answer.getContactNumber() + "");
            userJson.put("pdfLink", answer.getPdfLink() + "");
            userJson.put("digitalProfile", answer.getDigitalProfile()+ "");
           
            

            String body = userJson.toString().replaceAll("\\\\", "");
            System.out.println("body: " + body);
            conn.setReadTimeout(40000);
            conn.setConnectTimeout(40000);
            conn.setRequestMethod(method);
            conn.setDoInput(true);
            conn.setDoOutput(true);

//            System.out.println("----");
            String credentials = user + ":" + pass;
//            System.out.println(credentials);

            Base64.Encoder encoder = Base64.getEncoder();
            String base64EncodedCredentials = encoder.encodeToString(credentials.getBytes()); //base64 encoding for accessing URLs with authentication
            conn.setRequestProperty("Authorization", "Basic " + base64EncodedCredentials);

            String urlParameters = body;
            byte[] postData = urlParameters.getBytes(Charset.forName("UTF-8"));
            int postDataLength = postData.length;
            System.out.println("urlParameters: " + urlParameters);
            conn.setDoOutput(true);
            conn.setInstanceFollowRedirects(false);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("charset", "utf-8");
            conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
            conn.setUseCaches(false);
            System.out.println("postDataLength: " + postDataLength);
            try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
                wr.write(postData);
            }

             responseCode = conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            }
          
            System.out.println("adding digitest: " +response + " " + responseCode);

        } catch (JSONException | NumberFormatException | IOException e) {
            System.out.println("FAILED" + e.toString() + " " + response + conn.getContent().toString());
        }
          return responseCode;
    }
  
    
    
     public String executeUpload(String targetURL, String base64, String method, String email, String password) throws IOException, ParseException {
        HttpsTrustManager.allowAllSSL();

        String user = email;
        String pass = password;

        URL url = new URL(targetURL);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        InputStream is = null;
        String response = "";
        String contentAsString ="";
        try {
            JSONObject userJson = new JSONObject();

            String body = base64;
            conn.setReadTimeout(40000);
            conn.setConnectTimeout(40000);
            conn.setRequestMethod(method);
            conn.setDoInput(true);
            conn.setDoOutput(true);

//            System.out.println("----");
            String credentials = user + ":" + pass;
//            System.out.println(credentials);

            Base64.Encoder encoder = Base64.getEncoder();
            String base64EncodedCredentials = encoder.encodeToString(credentials.getBytes()); //base64 encoding for accessing URLs with authentication
            conn.setRequestProperty("Authorization", "Basic " + base64EncodedCredentials);

            String urlParameters = body;
            System.out.println("urlParameters: "+urlParameters);
            byte[] postData = urlParameters.getBytes(Charset.forName("UTF-8"));
            int postDataLength = postData.length;
            conn.setDoOutput(true);
            conn.setInstanceFollowRedirects(false);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("charset", "utf-8");
            conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
            conn.setUseCaches(false);
            try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
                wr.write(postData);
            }


            int responseCode = conn.getResponseCode();

            is = conn.getInputStream();

            contentAsString = convertInputStreamToString(is);
            System.out.println("responseCode: "+responseCode);
          
            if (responseCode != 200 || "".equals(contentAsString)) {
                conn.disconnect();
                return "";
            } else {
                conn.disconnect();
                return contentAsString;
            }


        } catch (Exception e) {
            conn.disconnect();
            e.printStackTrace();
            return "";
        } finally {
            if (is != null) {
                is.close();
            }
        }
    }
 
   


 

    public String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null) {
            result += line;
        }
        inputStream.close();
        return result;
    }

   

}