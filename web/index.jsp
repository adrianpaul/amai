<%-- 
    Document   : signin
    Created on : 04 22, 19, 4:02:32 PM
    Author     : Adrian Paul
--%>

<%@page import="Others.Constants"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!doctype html>
<!--[if IE 9]> <html class="ie9 no-js supports-no-cookies" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-js supports-no-cookies" lang="en"> <!--<![endif]-->
<head>
    <% 
            String invalidLogin = (String) request.getAttribute("invalidLogin");
            String responseCode = request.getAttribute("responseCode") + "";
            String expiredSession = (String) request.getAttribute("expiredSession");
            
            String invalidLogins = (String) session.getAttribute("invalidLogin");
            String responseCodes = session.getAttribute("responseCode") + "";
            String expiredSessions = (String) session.getAttribute("expiredSession");
            

    %>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="theme-color" content="#7db557">
    <link rel="canonical" href="https://materialize-shopify-themes.myshopify.com/account/login">
    <link rel="icon" type="image/png" href="https://www.globe.com.ph/etc/designs/globe/brie/headlibs/images/favicon.ico" />
    <title><%=Constants.appName%></title>

  


    <meta property="og:site_name" content="Materialize Shopify Themes">
    <meta property="og:url" content="https://materialize-shopify-themes.myshopify.com/account/login">
    <meta property="og:title" content="Account">
    <meta property="og:type" content="website">
    <meta property="og:description" content="Materialize Shopify Themes">
    <meta name="twitter:site" content="@materializecss">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Account">
    <meta name="twitter:description" content="Materialize Shopify Themes">

    <meta name="google-signin-scope" content="profile email">
    <meta name="google-signin-client_id" content=539554715527-k9lcjalrol1u1p8j829m1o5h7i6bo09c.apps.googleusercontent.com>
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    
    
    
    <link href="//cdn.shopify.com/s/files/1/2030/2737/t/6/assets/gallery-materialize.min.css?0" rel="stylesheet" type="text/css" media="all" />
    <link href="//cdn.shopify.com/s/files/1/2030/2737/t/6/assets/theme.scss.css?0" rel="stylesheet" type="text/css" media="all" />
    <!-- Lato Font -->
    <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <!-- Material Icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    
</head>
<body id="account" class="template-login ">

  <a class="in-page-link visually-hidden skip-link" href="#MainContent">Skip to content</a>

  <div id="shopify-section-header" class="shopify-section">


<!-- Navbar and Header -->

<nav class="nav-extended" style="background-color:#3c8dbc; height: 210px;" >


  <div class="nav-header center" style="text-transform: none;">
  

  
      <h1 class="tagline" style="margin-top:15px;text-transform: none;">AM ASSIGNMENT INQUIRY</h1>
    

  </div>
</nav>





</div>

  <main role="main" id="MainContent">
    <div class="section container">
  <div class="row">
    <div class="col s12 m6 offset-m3">
      <div class="card login-wrapper">
        <div class="card-content">
          <div id="CustomerLoginForm">
              <% if (request.getAttribute("invalidLogin") != null) {
                    if (invalidLogin == "true") { %>

                <div class="alert alert-danger" style=" opacity: 0.9;background-color: red;color:white;">
                    <strong>
                        <i class="ace-icon fa fa-times"></i>
                        Oops!
                    </strong>

                    You entered invalid login credentials. Please try again. (<%=responseCode%>)
                    <br>
                </div>
                <% } request.removeAttribute("invalidLogin");} %>
                <% if (request.getAttribute("expiredSession") != null ) {
                    if (expiredSession == "true") { %>

                <div class="alert alert-warning" style=" opacity: 0.9">
                    <strong>
                        <i class="ace-icon fa fa-times"></i>
                        Your session has expired!
                    </strong>

                    Please login again.
                    <br>
                </div>
                <% } request.removeAttribute("expiredSession");} %>
                
                <% if (session.getAttribute("invalidLogin") != null) {
                    if (invalidLogins == "true") { %>

                <div class="alert alert-danger" style=" opacity: 0.9;background-color: red;color:white;">
                    <strong>
                        <i class="ace-icon fa fa-times"></i>
                        Oops!
                    </strong>

                    You entered invalid login credentials. Please try again. (<%=responseCodes%>)
                    <br>
                </div>
                <% } request.removeAttribute("invalidLogin");} %>
                <% if (session.getAttribute("expiredSession") != null ) {
                    if (expiredSessions == "true") { %>

                <div class="alert alert-warning" style=" opacity: 0.9">
                    <strong>
                        <i class="ace-icon fa fa-times"></i>
                        Your session has expired!
                    </strong>

                    Please login again.
                    <br>
                </div>
                <% } request.removeAttribute("expiredSession");} %>
            <br><form action="SDValidateLogin" method="post">
                h4 class="center" style="color:#3c8dbc;">CHANGE PASSWORD</h4><br>

                <label for="email"> Email</label>
                <div class="input-field">
                    <input type="email" name="email" id="email" class="" placeholder="Enter Email Address">
                    <span> <i class="material-icons" style="position:absolute;margin-top:-53px;right:10px;">email</i></span>
                </div>

               <label for="password"> Password</label>
                <div class="input-field">
                    <input type="password" name="password" id="password" class="" placeholder="Enter Password">
                    <span> <i class="material-icons" style="position:absolute;margin-top:-53px;right:10px;">lock</i></span>  
                </div>
               <div class="row">
                    <div class="col offset-s2 s8 offset-m3 m6">
                        <input type="submit" class="btn-flat z-depth-0" value="Login" style="background-color:#3c8dbc;color:white;">
                    </div>
                    
               </div>
               <div style="margin-top: -12px;text-align: center;">
                    <a href="https://docs.google.com/a/globe.com.ph/forms/d/e/1FAIpQLSdiiC588uXS0rN6JcIEhM-95WAwCsnjIl14UWfW-zhuMXMKoA/viewform?c=0&w=1" target="_blank" id="main_login" > <ref>Need Access or Forgot Password?</ref></a>   <br>
               
               </div>   
            </form>
             <p style="text-align:center;margin-top: 10px;">OR</p><br>
               
            <div  style="text-align: center;">
                <div id="GoogleSigninButton" class="g-signin2 " data-width="200" data-onsuccess="onSignIn" data-theme="dark" style="  display: flex;  justify-content: center;"></div>
            </div>
                
          </div>

        </div>
      </div>
        
    </div>
  </div>
</div>



<div class="form-success hide" id="ResetSuccess">
  We&#39;ve sent you an email with a link to update your password.
</div>


  </main>

  <div id="shopify-section-footer" class="shopify-section"><footer class="page-footer">
  <div class="container">
  
  </div>
  <div class="footer-copyright">
    <div class="container">
        <center><small style="font-weight: bold;color:black;">Copyright &copy; 2019, <p>Materialize Shopify Themes. Powered by Shopify</p></small></center>
    </div>
  </div>
</footer>


</div>


  <!-- Javascript -->

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

  <script src="//cdn.shopify.com/s/assets/themes_support/option_selection-fe6b72c2bbdd3369ac0bfefe8648e3c889efca213baefd4cfb0dd9363563831f.js" type="text/javascript"></script>

  <script src="//cdn.shopify.com/s/assets/themes_support/api.jquery-e94e010e92e659b566dbc436fdfe5242764380e00398907a14955ba301a4749f.js" type="text/javascript"></script>

 
        <script>
            $.get("sidebarMenu.html", function(data){
                $("#sidebarMenu").replaceWith(data);
            });
        </script>
        

<script>
    setTimeout(function () {
        $('#GoogleSigninButton div div span span:last').text("Sign in with Google");
        $('#GoogleSigninButton div div span span:first').text("Sign in with Google");
        
        
    }, 100);
   
</script>        
<script>
    
  function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
      console.log('User signed out.');
    });
  }
  
  $(window).on('beforeunload', function(){
    // save the values to server side
    var auth2 = gapi.auth2.getAuthInstance();
   
    auth2.signOut().then(function () {
     console.log('User signed out.');
   });
});

$(window).on('load', function(){
    // restore values
     var auth2 = gapi.auth2.getAuthInstance();
   
    auth2.signOut().then(function () {
     console.log('User signed out.');
   });
});
</script>

<script>
    function onSignIn(googleUser) {
      // Useful data for your client-side scripts:
      var profile = googleUser.getBasicProfile();
      console.log("ID: " + profile.getId()); // Don't send this directly to your server!
      console.log('Full Name: ' + profile.getName());
      console.log("Image URL: " + profile.getImageUrl());
      console.log("Email: " + profile.getEmail());

      // The ID token you need to pass to your backend:
      var id_token = googleUser.getAuthResponse().id_token;
      console.log("ID Token: " + id_token);
      
        var datas = { idtoken :id_token }

         $.ajax({
            type: 'POST',
            url: "EmailSignIn",
            contentType: 'application/x-www-form-urlencoded; charset=utf-8',
            crossDomain:true,
            cache : true, 
            data: datas,
            success: function(d) {
                window.location = '/amai/home.jsp';
            }
        });
    }
</script>  

</body>
</html>
